<?php

class redirgo {
    public function common($str) {
        if (false !== (stripos($str, '<a')) && false !== (stripos($str, '</a>'))) {
            $noindex = Config::read('use_noindex');
            $url = 'http://'.$_SERVER['HTTP_HOST'].get_url('plugins/redirgo/go/?url=');

            $str = preg_replace("#\<a href=\"(http[s]*://[\w\d\-_.]*\.\w{2,}[\w\d\-_\\/.\?=\#;&%]*)\"([^\[]*)\>([^\[]*)\<\/a\>#iuU", '<a href="' . $url . '\\1" target="_blank" rel="nofollow">\\3</a>', $str);
        }

        return $str;
    }
}
?>
