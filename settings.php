<?php

$output    = '';
$template_patch = dirname(__FILE__).'/go/go.html';
$conf_pach = R.'plugins/before_smiles_parse_redirgo/config.json';

$config = json_decode(file_get_contents($conf_pach), true);

function config_write($set, $config, $conf_pach) {
    array_push($config, $set);
    print_r($config);
    file_put_contents($conf_pach, json_encode($config));
    return '<div class="warning">Сохранено!<br><br></div>';
}

if (isset($_POST['send'])) {
    $config['whitelist_sites'] = $_POST['whitelist_sites'];
    $config['blacklist_sites'] = $_POST['blacklist_sites'];
    $config['url_delay'] = $_POST['url_delay'];
    file_put_contents($conf_pach, json_encode($config));
    $output .= '<div class="warning">Сохранено!<br><br></div>';
}

$template = file_get_contents($template_patch);

$output    .=  '<style>
                .ib {font-weight: bold; font-style: italic;}
                .right {width: 50%;}
                input[type="text"] {height: auto}
                .list > .level1 > .items > .setting-item > .title {height: 40px;}
            </style>

            <form action="" method="post">
            <div class="list">
                <div class="title">Управление плагином RedirGo</div>
                <div class="level1">

                    <div class="items">
                        <div class="setting-item">
                            <div class="left">Задержка перед переходом по ссылке</div>
                            <div class="right">
                                <input type="text" size="100" name="url_delay" value="' . $config['url_delay'] . '"> <span class="comment2">Секунд</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="items">
                        <div class="setting-item">
                            <div class="left">Белый список сайтов<br><small>Домены, переход на которые осуществляется без задержек</small></div>
                            <div class="right">
                                <input type="text" size="100" name="whitelist_sites" value="' . $config['whitelist_sites'] . '"> <span class="comment2">Через запятую</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="items">
                        <div class="setting-item">
                            <div class="left">Черный список сайтов<br><small>Домены, на которые запрещен автоматический переход</small></div>
                            <div class="right">
                                <input type="text" size="100" name="blacklist_sites" value="' . $config['blacklist_sites'] . '"> <span class="comment2">Через запятую</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="items">
                        <div class="setting-item">
                            <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? htmlspecialchars($template) : '') . '</textarea>
                        </div>
                    </div>

                    <div class="items">
                        <div class="setting-item">
                            <div class="title" style="padding: 20px; text-align:center;"><input name="send" type="submit" value="Записать" class="save-button"></div>
                        </div>
                    </div>
                </div>
            </div>
            </form>';

?>