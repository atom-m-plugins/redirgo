<?php
if (empty($_GET['url'])) die();
$url = $_GET['url'];

function siteInList($site, $list) {
    if ($site && $list && is_array($list) && count($list) > 0) {
        foreach ($list as $item) {
            $pattern = '#^' . str_replace('*', '.*', str_replace('.', '\.', trim(mb_strtolower($item)))) . '$#i';
            if (preg_match($pattern, $site)) {
                return true;
            }
        }
    }
    return false;
}

include_once '../../../sys/boot.php';

$config = json_decode(file_get_contents(R.'plugins/redirgo/config.json'), true);

$whitelist = explode(',', $config['whitelist_sites']);
$blacklist = explode(',', $config['blacklist_sites']);
$delay = $config['url_delay'];
if (!$delay || $delay < 1) $delay = 10;

$in_white = false;
$in_black = false;

$info = parse_url($url);
if (isset($info['host'])) {
    $in_white = (mb_strtolower($info['host']) === mb_strtolower($_SERVER['SERVER_NAME']));
    if (!$in_white) {
        $site = trim(mb_strtolower($info['host']));
        $in_white = siteInList($site, $whitelist);
        $in_black = (!$in_white) ? siteInList($site, $blacklist) : false;
    }
}

if ($in_white) {
    header('Refresh: 0; url=' . $url);
} else {
    if (!$in_black) header('Refresh: ' . $delay . '; url=' . $url);
    $Viewer = new \Viewer_Manager(['template_path' => ROOT . '/plugins/redirgo/go/', 'layout' => false]);

    $markets = array('url' => $url, 'black' => $in_black, 'template_path' => get_url('/template/' . getTemplate()));

    echo $Viewer->parseTemplate('go.html.twig', $markets);
}
die();

?>
